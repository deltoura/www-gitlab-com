title: Signicat
file_name: Signicat
canonical_path: /customers/signicat/
cover_image: /images/blogimages/signicat_cover_image.jpg
cover_title: |
  Signicat reduces deploy time from days to just minutes
cover_description: |
  Software engineers at market-leading identity solutions provider rapidly move secure code into production using GitLab automation.
twitter_image: /images/blogimages/signicat_cover_image.jpg
twitter_text: Learn how Signicat reduces deploy time from days to just minutes with GitLab @signicat
customer_logo: /images/case_study_logos/signicat_logo.png
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Trondheim, Norway
customer_solution: GitLab Premium SaaS
customer_employees: |
  400+
customer_overview: |
  Quick development and deployment of reliable identity services was a must-have at Signicat. The company turned to GitLab Premium SaaS to power onboarding and drive collaboration. 
customer_challenge: |
  Rapidly growing company with fragmentation in tooling, manual processes which needed automation and governance that securely supports development and innovation.
key_benefits: >-

  
    Rapid pipeline deployment  

  
    Integrates with a multicloud environment 

  
    Scales with a high-growth organisation

  
    Fosters a collaborative culture 

  
    Facilitates business-critical industry certifications


    Robust integration capabilities 
customer_stats:
  - stat: 500 builds
    label: Accelerated daily delivery with 500 builds
  - stat:  100%
    label: 100% developer team scaling in 1 year (from 100 to 200)
  - stat:  Minutes
    label: From days to just minutes  – rapid pipeline deployment
customer_study_content:
  - title: the customer
    subtitle: A market leading European digital identity solutions provide
    content: >-
  
       [Signicat](https://www.signicat.com)  is a market leading European digital identity solutions provider that has aggressively extended its capabilities, both organically and through acquisitions, to create a full-line of identity management tooling, including identity proofing solutions, a suite of electronic signature solutions, and advanced authentication. Clients, partners and customers include Fortune 500 banks and financial services, government and health services, payments, insurance and other industries. While supporting more than 30 national identity methods, Signicat enables digital transformation in regulated and unregulated industries. The company’s software services focuses on essential use cases, including trustworthy digital onboarding and reboarding, identity verification and authentication, risk and compliance management, and fraud detection. Capabilities have been expanded, just as the threat surfaces of modern B2B and B2C applications have expanded. This is particularly crucial as clients focus their efforts on better knowing, identifying and protecting their customers, especially during the COVID-19 pandemic. Advanced use of cloud-native software development methods underlies Signicat’s efforts to meet the evolving needs for ever-better digital identity technologies and building a trusted digital world. 


  - title: the challenge
    subtitle:  Achieving rapid, secure deployment - while improving service deliveries
    content: >-
    

        The challenge Signicat faced were manual processes which needed to be automated, and fragmentation across tooling in different teams. Signicat also needed a solution to continually expand as the business grew, which called for an exceptionally cohesive integration architecture – one that must adhere to the strictest standards of governance and compliance. The need to continue promoting collaboration across diverse working teams was also important. It was important to know the status of work, and which person or team could help on specific code dependencies. 


        Development leaders looked for an overarching platform that allowed them to move quickly, and to, in effect, accomplish more with less. What they needed was a DevOps platform that future-proofed them and aligned with their ambitious roadmap. To get there, they went with GitLab to achieve rapid, dependable, development cycles and scalable performance through various peaks of operations. 



  - title: the solution
    subtitle:  GitLab provides scalability, visibility and enhanced integration to power Signicat initiatives
    content: >-


        GitLab Premium SaaS, ideally suited for scaling organizations and multi-team use, is used to improve the developer experience, while enhancing productivity. Developers are onboarded quickly, enabling them to get important workloads up and running. Scalable, reliable, and predictable build timetables also help fine-tune operations. GitLab enables integration with a variety of static application security testing and other popular third-party tools, which is especially crucial to a company that routinely adds to its technology portfolio via an aggressive acquisition strategy. The platform provides visibility into project status, and supports cross-team communications. This has given developer teams more empowerment and autonomy, which had been another key objective for Signicat. Improvements in visibility also took the form of fine-grained code documentation, used for audits as part of necessary certifications and verifications. The platform also meshed well with the company’s move to containerization of software services.

            

  - title: the results
    subtitle:  Huge gains in deployment times and culture alignment
    content: >-

        GitLab provides developer teams a platform for reliable, scalable and predictable build times. Collaboration, resourcing and planning have improved as well. "A positive thing we are seeing is in terms of GitLab support. Instead of having to set aside resources internally, we can direct our internal users to GitLab support" added Jon. One of the most noticeable and dramatic improvements since adopting GitLab is that Signicat’s deployment time has dropped from days to just minutes. It has significantly changed not only their speed to deployment but it has supported Signicat with the ability to be agile and deliver more continuously. GitLab’s dedication to transparency and openness in its technology roadmap is a notable plus for planning, according to Jon Skarpeteig, Signicat’s Vice-President for Cloud Architecture. "The push from the company perspective is towards containerizing everything. That is one of the reasons why GitLab was a good fit for us. It really solves containers in a very elegant way" he explains. That transparent GitLab roadmap matches Signicat’s own roadmap, providing an integrated system for source code management, continuous integration and continuous delivery of key services – including Docker and Kubernetes. 


        “GitLab’s open culture has served us well. There is an openness about what is coming and how things get prioritized,” said Skarpeteig, adding that GitLab support is another benefit. GitLab’s support resources for Signicat’s internal developers’ free up development managers’ time. In addition, GitLab’s software supports the team’s ability to quickly react to change; which is extremely valuable for a company in rapid growth at the same time it is at the center of important trends in modern commerce and technology.


   

        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/features/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: The ability to react to change quickly is extremely valuable for a scaling company like ourselves. We are seeing a lot of collaboration around GitLab CI/CD and pipelines.
    attribution: Jon Skarpeteig
    attribution_title: Vice-President for Cloud Architecture, Signicat











