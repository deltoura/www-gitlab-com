---
layout: markdown_page
title: "Category Direction - Component Catalog"
description: "Reusable pipeline configuration and definitions" 
canonical_path: "/direction/verify/component_catalog/"
---

- TOC
{:toc}


## Component Catalog

|                       |                               |
| -                     | -                             |
| Stage | [verify](/direction/verify/) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2023-06-04` |

### Introduction and how you can help
Thanks for visiting this direction page on the Components catalog category at GitLab. This page belongs to the [Pipeline Authoring Group](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-authoring/) within the Verify Stage and is maintained by Dov Hershkovitch. You can submit your feature requests using [GitLab issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/new).

## Overview
We aim to foster a collaborative community of developers who can easily share, build, and maintain high-quality CI/CD configurations. By providing a platform that serves as a centralized hub for managing all DevOps-related assets within an organization, we aim to empower developers to focus on true innovation and unlock the full potential of the open-source ecosystem. Through our commitment to inclusivity, transparency, and accessibility, we strive to create a continuous learning and improvement culture where every community member can contribute.
### Strategy and Themes
DevOps is all about speed, it delivers value faster by shortening the software development life cycle. Organizations that want to accelerate their DevOps adoption need to set up a working pipeline so other teams can use it to automate their workflow. During the development of a pipeline configuration, engineers frequently encounter the following challenges::
1. "Where can I find it?" - Whenever engineers embark on setting up a new pipeline, they often search for existing pipelines within their organization or externally. This is because someone, at some point, has likely attempted to configure a similar pipeline."
2. "How do I use it?" - Once engineers locate the suitable pipeline to work with, they require clear instructions on effectively utilizing it. This guidance is crucial for understanding the pipeline's functionalities and ensuring its proper usage.
3. "Does it always work as expected?" - Engineers often question whether the pipeline is consistently reliable and delivers the expected results. They also evaluate its reusability and whether it can be shared effectively with other teams within the organization. These aspects are vital for determining the pipeline's suitability for wider adoption and collaboration.
While it is possible to solve each problem separately, GitLab believes that we need to solve those problems holistically by building a framework that contains tools that add functionality and improve your CI workflow, aspiring to provide the best-in-class experience for building and maintaining CI/CD pipelines. Our strategy is to provide an opinionated framework that acts as:
1. The SSOT allows users to browse and search for the desired pipeline components.
2. Contains shareable and reusable package components that could be attached to any CI configuration.
3. An easy way to document the usage of each component.
4. Foster a collaborative methodology by allowing users to contribute to the catalog by developing and publishing their components.
We should adopt a bottom-up approach to construct this solution, starting with the smallest building blocks and gradually progressing upwards. By starting from the foundational elements and gradually building upon them, we can create a robust and cohesive solution.
### 1 year plan

| Track | Feature| Details |  status|
| ------ | ------ | ------ |  ---|
| 1 |Deliver input interpolation as `Beta`| Allow users to constract a resusable pipeline configuration|Completed
| 2 |Deliver experimental [CI/CD components](https://gitlab.com/groups/gitlab-org/-/epics/9409) |Component is the smallest building block which constetute a Catalog | Completed
| 3 | Release experimental [CI/CD Components Catalog - index page](https://gitlab.com/gitlab-org/gitlab/-/issues/359047/) |single page listing all available components in an organization| in dev
| 4 | [Restracture components](https://gitlab.com/groups/gitlab-org/-/epics/10728)|Restracture component to acomodate to an existing custmoer stracture | Planning breakdown
| 5 | Release experimental [CI/CD Components Catalog - detailed page](https://gitlab.com/gitlab-org/gitlab/-/issues/392738)| Detailed view of a component| In dev
| 6 | [Input interpolation improvements](https://gitlab.com/groups/gitlab-org/-/epics/10603)|Input enhancements |In dev|
| 7 |Make inputs/interpolation a GA feature. | | Planning breakdown
| 8 | Introduce [GitLab steps](https://gitlab.com/gitlab-org/gitlab/-/issues/357669) | A step would be the script that the runner will run | Planning breakdown
| 9 |Make CI/CD catalog/components `Beta`. Improved release workflow, improved Catalog UX, and stable directory structure. | | Planning breakdown
| 10 | [Visability to Components usage](https://gitlab.com/gitlab-org/gitlab/-/issues/393326) | Provide an analytic dashboard to component usage| Planning breakdown
| 11 | Create an [instance wide catalog](https://gitlab.com/gitlab-org/gitlab/-/issues/411584)| Could evolve to become the Community catalog |Not started|

#### AI improvments
* To address the misperception surrounding [AI DevOps](https://gitlab.com/gitlab-org/gitlab/-/issues/408839) GitLab proposes including it as a CI component in the CI/CD Components Catalog. This move aims to improve visibility and showcase the robustness of AI DevOps within the GitLab DevSecOps Platform. By positioning it as a starting point for DevOps transformation, organizations can discover and leverage the comprehensive and opinionated nature of Auto DevOps. By incorporating Auto DevOps as a CI component, GitLab aims to engage users and encourage them to explore its powerful features for their CI/CD workflows.
* [Intellegent search powered by generative AI](https://gitlab.com/gitlab-org/gitlab/-/issues/412663) - Unlike traditional keyword-based searches, Intelligent Search understands the intent behind user queries, enabling it to provide more accurate and relevant results. In addition, we can leverage AI to prompt more information about specific inputs required by the user, which will result in configured and fine-tuned components with the relevant inputs. This could enhance the user experience and streamline the process of selecting, configuring, and using components from the catalog. 

#### What is next for us
Over the next milestones, we will be focusing on answering the top three problems mentioned above
1. CI/CD catalog (MVC) - [Index page](https://gitlab.com/gitlab-org/gitlab/-/issues/359047/) - Single page listing all available components in an organization ("Where can i find it"?)
2. CI/CD catalog - [details page](https://gitlab.com/groups/gitlab-org/-/epics/10428) - Details page will surface additional information for each selected component ("How can I use it"?)
3. [Input interpolation improvements](https://gitlab.com/groups/gitlab-org/-/epics/10603) - We strongly advocate for utilizing inputs as the preferred approach for injecting parameters into a pipeline configuration. Embracing this capability empowers users to construct reusable pipeline configurations while promoting a deterministic behavior ("Does it always works as expected"?).
In addition, we'll continue the discussion with internal team as they dogfood some of our popular CI/CD [template to component](https://gitlab.com/gitlab-org/gitlab/-/issues/390656)

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

#### What we recently completed
1. [Input interpolation](https://about.gitlab.com/releases/2023/04/22/gitlab-15-11-released/#define-inputs-for-included-cicd-configuration)
2. [CI/CD components - experimental](https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#cicd-components)

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecasted near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

#### Key Capabilities 
1. Centralized Component Catalog: Create a centralized component catalog that serves as a single source of truth for users to browse and search for pipeline components. The catalog should be easily accessible and provide comprehensive information about each component, including its purpose, usage, and documentation.

2. Shareable and Reusable Components: Enable users to develop and publish their pipeline components to the catalog. Components should be designed to be shareable and reusable, allowing developers to easily attach them to their CI/CD configurations. This promotes collaboration and reduces duplication of effort within the organization.

3. Versioning: Implement versioning for components to ensure that users can pin their pipelines to a specific revision of a component. This helps in maintaining stability and allows for controlled updates. 

4, Component Abstraction and Encapsulation: Encourage the development of components that abstract away complex pipeline configuration units. Components should encapsulate implementation details and provide a clear interface, enabling users to use them without needing to know the underlying details. 

5. Documentation: Provide a mechanism for users to document the usage of each component. This should include clear guidelines, examples, and best practices to help users understand how to utilize the components effectively. 

6. Collaborative Contribution: Foster a collaborative community where developers can contribute to the component catalog by developing and publishing their own components. Implement mechanisms for peer review, feedback, and contribution management to ensure the quality and reliability of the catalog assets.

7. Accessibility: Design the platform to be inclusive and accessible to all community members. Provide intuitive interfaces, clear navigation, and comprehensive search capabilities, making it easy for users to find the components they need. 

8. Continuous Improvement and Learning: Establish a culture of continuous learning and improvement within the community. Encourage feedback from users, collect analytics on component usage, and iterate on the catalog based on user needs and preferences. Regularly update and enhance the catalog with new components, improved documentation, and additional features based on community contributions.


## Dogfooding
The best way to understand how GitLab works is to use it for as much of your job as possible, this is why we practice [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding) We have recently begun collaborating with our internal teams in GitLab which express their desire to dogfood some of our features: 
* Productivity team - this effort is tracked through this [collaboration issue](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/141), and the team is currently identifying commonly used CI/CD templates and attempting to convert them into components. Our goal is to understand our customers' challenges when converting their templates into components. 
* Incubation team - this effort is tracked through this [collaboration issue](https://gitlab.com/gitlab-org/gitlab/-/issues/399480)
* Secure - We've converted some of our existing templates such as SASTS, Secret detection and more, this effort is tracked through this [collabortion issue](https://gitlab.com/gitlab-org/gitlab/-/issues/390656)

## Competitive Landscape
Notable competitors in this space are:

- GitHub actions with their [actions marketplace](https://github.com/marketplace?category=&query=&type=actions&verification=)
- [Circle CI orbs](https://circleci.com/developer/orbs)
- CodeFresh also provide their users with a [CI step library](https://codefresh.io/steps/)

Watch this walkthrough video of the different contribution frameworks available by these competitors:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/7WSWGDtMD7A" frameborder="0" allowfullscreen="true"> </iframe>
</figure>



## Glossary
This section defines the terminology throughout this project. With these terms we are only identifying abstract concepts and are subject to changes as we refine the design by discovering new insights.

* **Component** The reusable unit of pipeline configuration.
* **Project** The GitLab project attached to a repository. A project can contain multiple components.
* **Catalog** The collection of projects that are set to contain components, which will be broken down into two flavors:
  * **Private catalog** Scoped per namespace
  * **Public catalog** Single source of truth
* **Version** The release name of a tag in the project, which allows components to be pinned to a specific revision.

### Components catalog
The component catalog will be a collection of components which:

* Is the SSOT for users to browse and search for the pipeline components they desire.
* Contains shareable and reusable packages of YAML configurations (components) that could be attached to any CI/CD configuration.
* Has an easy way to document usage of each component.
* Allows users to contribute to the catalog by developing and publishing their components.

### Pipeline component
A pipeline component is a reusable single-purpose building block that abstracts away a single pipeline configuration unit. Components are used to compose a part or entire pipeline configuration. It can optionally take input parameters and set output data to be adaptable and reusable in different pipeline contexts while encapsulating and isolating implementation details.

Components allow a pipeline to be assembled by using abstractions instead of having all the details defined in one place. Therefore, when using a component in a pipeline, a user shouldn't need to know the implementation details of the component and should only rely on the provided interface.

A pipeline component defines its type which indicates in which context of the pipeline configuration the component can be used. For example, a component of type X can only be used according to the type X use-case.

For best experience with any systems made of components, it's fundamental that components are:

* Single purpose: A component must focus on a single goal and the scope must be as small as possible.
* Isolated: When a component is used in a pipeline, its implementation details should not leak outside the component itself into the main pipeline.
* Reusable: A component is designed to be used in different pipelines. Depending on the assumptions it's built on, a component can be more or less generic. Generic components are more reusable but may require more customization.
* Versioned: When using a component we must specify the version we are interested in. The version identifies the exact interface and behavior of the component.
* Resolvable: When a component depends on another component, this dependency must be explicit and trackable.

Some of those best practices will be enforced in the product while others would need to be enforced by the user.
